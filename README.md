Blog creado a partir de tutos
- https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
- https://gohugo.io/getting-started/quick-start/

Link público: https://saius.gitlab.io/hugo-blog-test/

Link dev: http://localhost:1313/hugo-blog-test/

## Comandos
Arrancar servidor dev:

`hugo server`

Crear nuevo post:

`hugo new posts/NOMBRE.md`

## Creando el sitio
Los pasos para crear este sitio fueron los siguientes:
```
sudo pacman -S hugo

hugo new site hugo-blog-test
cd hugo-blog-test

git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
echo theme = \"ananke\" >> config.toml

hugo new posts/my-first-post.md
# cambiar en header el parámetro draft por false (para gitlab ci)
nano content/posts/my-first-post.md

# para ver en servidor local
hugo server #-D

# copiar el código del tuto de hugo + gitlab pages
nano .gitlab-ci.yml
# editar baseURL (https://saius.gitlab.io/hugo-blog-test/ en este caso)
nano config.toml
echo "/public" >> .gitignore

# de yapa, cambiar el idioma, agregar la siguiente config:
# defaultContentLanguage = 'es'
nano config.toml

# (usando un alias de ssh como host)
git remote add origin git@git-as-saius:saius/hugo-blog-test.git
git push -u origin main
```

Además tuve que ir a **Settings > General** del gitlab y bajo **Visibility, ...** cambiar **Pages** a **Public** (o sino más amplio **Project visibility** a **Public**)
