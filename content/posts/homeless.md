---
title: "Homeless"
date: 2022-08-28T16:40:19-03:00
draft: false
featured_image: "img/homeless.png"
---

Sin casa, sin territorio, sin nación.

¿Acceso a la tierra o a la vivienda? Te la debo. No puedo depender del estado; entonces dependo
del capital, o de ofertar mi trabajo, mi cuerpo.

Voluntariados, alquileres temporarios o por día, monoambientes, departamentos amoblados; poner
la carpa donde primero se me ocurra, prender un fueguito, una harinita, sale cha pati.

Tengo un cuarto allá y acá que me podrían hospedar, gratis. Pero conviviría con gente que no 
me llevo del todo bien, no tendría mi privacidad. Tantas cuestiones sin resolver.

No tengo casa. No tengo donde reposar. El mundo es hostil. Así es naturalmente pero ya me
comí la mentira del capitalismo en mis primeros años de vida y ahora no puedo aceptar esta
dura (no triste) realidad.

En una aldea-tribu hay que laburar, salir a cazar, recolectar, sembrar, arreglar las carpas-casas,
o construirlas. Estarías dando laburo por hospedaje digno, en términos capitalistas.

Y entonces ¿qué es lo que espero? ¿qué es lo que deseo?

Vivir tranquilo, con wifi y cocina, acompañado no me molestaría siempre que sea buena-sana
compañía. Necesito reposo, que no tengo. Quiero estar tranquilo, tener resuelto el tema
de vivienda y trabajo. Quiero, necesito, un trabajo, una fuente de plata, de capital,
un ingreso, un billete en mano. Triste pero cierto; tengo que comer. O mejor dicho, en el
futuro tengo que comer, porque hoy por hoy tengo plata, pero se escurre y algún día acaba,
como un reloj de arena.

No tengo habilidad económica, además estoy frustrado, tenso, y eso no ayuda.

Tengo que salir adelante, como mierda sea, o preferiblemente "de la mejor manera".
No sé qué mierda hacer. Se me comen los días (y las uñas). Pero bueno, morir no voy a morir,
eso seguro, aunque a veces no alcanza solo con eso...
