---
title: "Hellsing"
date: 2022-08-21T05:58:59-03:00
draft: false
featured_image: 'images/cap2/vlcsnap-2022-08-20-22h01m23s968.png'
---

__NOTA: al final esto es Hellsing "Ultimate" y no la serie original, pero igual está buenísima
y la recomiendo. saludos desde el futuro del pasado__

# Magnet

Audio: JP   
Subs: EN

[CBM] Hellsing Ultimate 1-10 Complete (Dual Audio) [BDRip-1080p-8bit-AC3]

[magnet:?xt=urn:btih:bkwmln5mjiuv33nljwd5f4vnwwfl3zs7&dn=%5BCBM%5D%20Hellsing%20Ultimate%201-10%20Complete%20%28Dual%20Audio%29%20%5BBDRip-1080p-8bit-AC3%5D&xl=24859461155&fc=11](magnet:?xt=urn:btih:bkwmln5mjiuv33nljwd5f4vnwwfl3zs7&dn=%5BCBM%5D%20Hellsing%20Ultimate%201-10%20Complete%20%28Dual%20Audio%29%20%5BBDRip-1080p-8bit-AC3%5D&xl=24859461155&fc=11)

# Cap1
![alt text](images/cap1/vlcsnap-2022-08-21-20h44m39s970-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h45m17s177-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h49m21s734-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h49m39s253-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h49m49s515-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h50m52s949-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h53m31s374-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h53m47s670-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h55m16s896-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h55m20s510-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h56m33s386-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h57m13s964-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h58m09s444-min.png)
![alt text](images/cap1/vlcsnap-2022-08-21-20h58m22s399-min.png)

# Cap2
![alt text](images/cap2/vlcsnap-2022-08-20-22h01m23s968-min.png)
![alt text](images/cap2/vlcsnap-2022-08-20-22h01m30s630-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h26m02s439-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h26m13s857-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h26m46s821-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h27m23s412-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h28m24s818-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h29m20s009-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h29m25s057-min.png)
![alt text](images/cap2/vlcsnap-2022-08-21-01h30m31s627-min.png)

# Cap3
![alt text](images/cap3/vlcsnap-2022-08-22-01h49m39s397-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-01h56m59s175-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h03m10s192-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h05m59s011-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h06m03s558-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h10m24s264-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h17m41s488-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h17m45s572-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h17m46s495-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h27m11s463-min.png)
![alt text](images/cap3/vlcsnap-2022-08-22-02h27m23s517-min.png)

# Cap4
![alt text](images/cap4/vlcsnap-2022-08-23-01h30m18s370-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h30m41s830-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h34m32s920-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h38m34s593-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h41m19s899-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h42m45s694-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h43m20s435-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h44m31s418-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h45m03s052-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h47m11s683-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h47m27s317-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h49m19s006-min.png)
![alt text](images/cap4/vlcsnap-2022-08-23-01h55m05s523-min.png)
